# nesci -- neuronal simulator conan interface

[License](https://devhub.vr.rwth-aachen.de/VR-Group/nesci/blob/master/LICENSE)

[Latest Version](https://devhub.vr.rwth-aachen.de/VR-Group/nesci)

![Build Status](https://devhub.vr.rwth-aachen.de/VR-Group/nesci/badges/master/build.svg)

# Introduction

Nesci is designed as a conan interface for neuronal simulators such as NEST.
It is split into a producer and a consumer part .

Nesci has been cross-developed for both Windows and Unix systems.

## Supported compilers:
  - msvc (tested with msvc++14.1) 
  - gcc (tested with gcc 5.3.1, 6.3.1 and 7.3.1)
  - clang (tested with clang 9.1)
  
Other compilers versions might work as well. 

## The different modules it includes:
  - Producer
    - The Producer records data received from different multimeters (NEST or Arbor) as well as spike detectors.
	
  - Consumer
    - The Consumer receives the data recorded by the Producer, it also functions for the same multimeters and spike detectors.
	- It has functionality to read out the timesteps for datapoints or a whole series of data.
	
    - The Consumer and Producer can also be used with a Python module by setting:
	cmake -PYTHON_ENABLE_MODULE__pyconsumer=True
	cmake -PYTHON_ENABLE_MODULE__pynesci=True
	- or setting those options to true in the CMake GUI

# API
## Consumer
Every class is inside the namespace `nesci::consumer`.

**SpikeDetectorDataView**
```c++
class SpikeDetectorDataView {
  conduit::float64_array GetTimesteps() const;
  conduit::uint64_array GetNeuronIds() const;
};
```
SpikeDetectorDataView gives access to two arrays with the same number of entries. The length of these arrays represent the number of spikes that are described in the dataset. So, GetTimesteps()[i] represents the simulation time and GetNeuronIds()[i] represents the neuron id of the `i`th spike described in the dataset. *The data is not guaranteed to be sorted in any way.*

**NestMultimeterDataView**
```c++
class NestMultimeterDataView {
  double GetTimestep() const;
  conduit::uint64_array GetNeuronIds() const;
  std::vector<std::string> GetIntegerAttributeNames() const;
  std::vector<std::string> GetFloatingPointAttributeNames() const;
  conduit::int64_array GetIntegerAttributeValues(
      const std::string& attribute
  ) const;
  conduit::float64_array GetFloatingPointAttributeValues(
      const std::string& attribute
  ) const;
};
```
NestMultimeterDataView gives access to the data recorded by a nest multimeter. One dataset describes the values of multiple attributes of multiple neurons *for a single timestep*.

The timestep can be retreived using `GetTimestep()`. The IDs of the nereurons whose attributes values are contained in this dataset can be retrieved using `GetNeuronIds()`. The names of the attributes stored in the dataset can be retrieved using `GetIntegerAttributeNames()` and `GetFloatingPointAttributeNames()` depending on their type. Finally, the values of each neuron for one attribute can be retrieved using `GetIntegerAttributeValues()` or `GetFloatingPointAttributeValues()` again depending on their type. The value with index `i` again corresponds to the neuron id with index `i` returned from `GetNeuronIds()`.
	
# Building nesci

## Dependencies
The version numbers specify the versions used for testing. Other versions may also work.

### Required
  - [CMake](https://cmake.org/) (v3.6.0 or higher)
  - [Conduit](https://github.com/LLNL/conduit) (v0.3.1)

### Optional
When you want the python bindings (`-DWITH_PYTHON_BINDINGS=ON`, `OFF` by default) you will also need the following:
  - [Boost.Python](https://www.boost.org/users/download/)
  - [Python](https://www.python.org/)

If you want to enable the tests (`-DENABLE_TESTS=ON`, `ON` by default) you will also need:
  - [Catch2](https://github.com/catchorg/Catch2)

If you want to to contribute to the library you want to have the developer tests enabled (`-DENABLE_DEVELOPER_TESTS=ON`, `OFF` by default) and you will also need:
  - [Cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint)
  - [Cppcheck](https://sourceforge.net/projects/cppcheck/)

## Building nesci

1. Build all dependencies you need and install them to a directory `$DEPENDENCY_DIR`.
2. Create a build directory and run CMake from there `cmake [FLAGS] -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY] [PATH_TO_NESCI_SOURCE]`. Where `[INSTALL_DIRECTORY]` is the directory where the library should be installed and `[PATH_TO_NESCI_SOURCE]` is the path to the root of this repository. The values for `[FLAGS]` depends on your configuration (see below).
3. Run `make [-j]` to build the library.
4. Run `make test` to check if everything went fine (only if `ENABLE_TESTS` was set to `ON` default: `ON`).
5. Run `make install` to install the library.

### Minimal build
`cmake -DCONDUIT_DIR=$DEPENDENCY_DIR -DENABLE_TESTS=OFF -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY] [PATH_TO_NESCI_SOURCE]`

## Using nesci

```
find_package(nesci REQUIRED producer consumer)
# ...
target_link_libraries(... nesci_producer nesci_consumer)
```
See `test_package/CMakeLists.txt` for an example.