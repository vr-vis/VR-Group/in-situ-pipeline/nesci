#include "nesci/consumer/consumer.hpp"
#include "nesci/producer/producer.hpp"

#include <iostream>

int main() {
  nesci::producer::Greet();
  std::cout << std::endl;
  nesci::consumer::Greet();
  std::cout << std::endl;
}