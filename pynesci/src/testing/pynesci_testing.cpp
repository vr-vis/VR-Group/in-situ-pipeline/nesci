//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "pynesci/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#if __GNUC__ >= 7
#pragma GCC diagnostic ignored "-Wregister"
#endif
#include "boost/python.hpp"
SUPPRESS_WARNINGS_END
#include "nesci/testing/data.hpp"

namespace pynesci {
namespace testing {

using boost::noncopyable;
using boost::python::args;
using boost::python::bases;
using boost::python::class_;
using boost::python::def;
using boost::python::enum_;
using boost::python::init;
using boost::python::list;
using boost::python::no_init;
using boost::python::pure_virtual;
using boost::python::scope;
using boost::python::wrapper;

SUPPRESS_WARNINGS_BEGIN

template <typename T>
list VectorToList(const std::vector<T>& vector) {
  list python_list;
  for (const auto& value : vector) {
    python_list.append(value);
  }
  return python_list;
}

// cppcheck-suppress unusedFunction
BOOST_PYTHON_MODULE(_pynesci_testing) {
  class_<conduit::Node>("Node");

  scope().attr("ANY_MULTIMETER_NAME") = nesci::testing::ANY_MULTIMETER_NAME;
  scope().attr("ANY_DOUBLE_ATTRIBUTES") =
      VectorToList(nesci::testing::ANY_DOUBLE_ATTRIBUTES);
  scope().attr("ANY_LONG_ATTRIBUTES") =
      VectorToList(nesci::testing::ANY_LONG_ATTRIBUTES);
  scope().attr("ANY_TIME") = nesci::testing::ANY_TIME;
  scope().attr("OTHER_TIME") = nesci::testing::OTHER_TIME;
  scope().attr("ANY_ID") = nesci::testing::ANY_ID;
  scope().attr("OTHER_ID") = nesci::testing::OTHER_ID;

  scope().attr("ANY_DOUBLE_VALUES") =
      VectorToList(nesci::testing::ANY_DOUBLE_VALUES);
  scope().attr("OTHER_DOUBLE_VALUES") =
      VectorToList(nesci::testing::OTHER_DOUBLE_VALUES);
  scope().attr("ANY_LONG_VALUES") =
      VectorToList(nesci::testing::ANY_LONG_VALUES);
  scope().attr("OTHER_LONG_VALUES") =
      VectorToList(nesci::testing::OTHER_LONG_VALUES);
  scope().attr("ANY_SPIKE_DETECTOR_NAME") =
      nesci::testing::ANY_SPIKE_DETECTOR_NAME;

  scope().attr("NestMultimeterDataNode") =
      nesci::testing::CreateNestMultimeterDataNode();

  scope().attr("SpikeDetectorDataNode") =
      nesci::testing::CreateSpikeDetectorDataNode();
}
SUPPRESS_WARNINGS_END

}  // namespace testing
}  // namespace pynesci
