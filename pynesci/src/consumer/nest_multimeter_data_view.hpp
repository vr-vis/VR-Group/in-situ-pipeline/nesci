//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <string>

#include "nesci/consumer/nest_multimeter_data_view.hpp"
#include "pyconsumer.hpp"
#include "pynesci/suppress_warnings.hpp"

namespace pynesci {
namespace consumer {

SUPPRESS_WARNINGS_BEGIN

boost::python::list GetNestMultimeterNeuronIds(
    nesci::consumer::NestMultimeterDataView* multimeter) {
  boost::python::list ret_val;
  const auto neuron_ids = multimeter->GetNeuronIds();
  for (conduit::index_t i = 0, count = neuron_ids.number_of_elements();
       i < count; ++i) {
    ret_val.append(neuron_ids[i]);
  }
  return ret_val;
}

boost::python::list GetNestMultimeterIntegerAttributeNames(
    nesci::consumer::NestMultimeterDataView* multimeter) {
  boost::python::list ret_val;
  for (const auto& data : multimeter->GetIntegerAttributeNames()) {
    ret_val.append(data);
  }
  return ret_val;
}

boost::python::list GetNestMultimeterFloatingPointAttributeNames(
    nesci::consumer::NestMultimeterDataView* multimeter) {
  boost::python::list ret_val;
  for (const auto& data : multimeter->GetFloatingPointAttributeNames()) {
    ret_val.append(data);
  }
  return ret_val;
}

boost::python::list GetNestMultimeterIntegerAttributeValues(
    nesci::consumer::NestMultimeterDataView* multimeter,
    const std::string& attribute) {
  boost::python::list ret_val;
  const auto int_attributes = multimeter->GetIntegerAttributeValues(attribute);
  for (conduit::index_t i = 0, count = int_attributes.number_of_elements();
       i < count; ++i) {
    ret_val.append(int_attributes[i]);
  }
  return ret_val;
}

boost::python::list GetNestMultimeterFloatingPointAttributeValues(
    nesci::consumer::NestMultimeterDataView* multimeter,
    const std::string& attribute) {
  boost::python::list ret_val;
  const auto float_attributes =
      multimeter->GetFloatingPointAttributeValues(attribute);
  for (conduit::index_t i = 0, count = float_attributes.number_of_elements();
       i < count; ++i) {
    ret_val.append(float_attributes[i]);
  }
  return ret_val;
}

void ExposeNestMultimeterDataView() {
  class_<nesci::consumer::NestMultimeterDataView,
         bases<nesci::consumer::DeviceDataView>>("NestMultimeterDataView",
                                                 init<const conduit::Node*>())
      .def("IsValid", &nesci::consumer::NestMultimeterDataView::IsValid)
      .def("GetTimestep", &nesci::consumer::NestMultimeterDataView::GetTimestep)
      .def("GetNeuronIds",
           &nesci::consumer::NestMultimeterDataView::GetNeuronIds)
      .def("GetIntegerAttributeNames", &GetNestMultimeterNeuronIds)
      .def("GetFloatingPointAttributeNames",
           &GetNestMultimeterFloatingPointAttributeNames)
      .def("GetIntegerAttributeValues",
           &GetNestMultimeterIntegerAttributeValues)
      .def("GetFloatingPointAttributeValues",
           &GetNestMultimeterFloatingPointAttributeValues);
}

SUPPRESS_WARNINGS_END

}  // namespace consumer
}  // namespace pynesci
