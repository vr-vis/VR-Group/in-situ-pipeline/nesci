//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <string>

#include "nesci/consumer/spike_detector_data_view.hpp"
#include "pyconsumer.hpp"
#include "pynesci/suppress_warnings.hpp"

namespace pynesci {
namespace consumer {

SUPPRESS_WARNINGS_BEGIN

boost::python::list GetSpikeDetectorTimesteps(
    nesci::consumer::SpikeDetectorDataView* spike_detector) {
  boost::python::list ret_val;
  const auto timesteps = spike_detector->GetTimesteps();
  for (conduit::index_t i = 0, count = timesteps.number_of_elements();
       i < count; ++i) {
    ret_val.append(timesteps[i]);
  }
  return ret_val;
}

boost::python::list GetSpikeDetectorNeuronIds(
    nesci::consumer::SpikeDetectorDataView* spike_detector) {
  boost::python::list ret_val;
  const auto neuron_ids = spike_detector->GetNeuronIds();
  for (conduit::index_t i = 0, count = neuron_ids.number_of_elements();
       i < count; ++i) {
    ret_val.append(neuron_ids[i]);
  }
  return ret_val;
}

void ExposeSpikeDetectorDataView() {
  class_<nesci::consumer::SpikeDetectorDataView,
         bases<nesci::consumer::DeviceDataView>>("SpikeDetectorDataView",
                                                 init<const conduit::Node*>())
      .def("IsValid", &nesci::consumer::SpikeDetectorDataView::IsValid)
      .def("GetTimesteps", &GetSpikeDetectorTimesteps)
      .def("GetNeuronIds", &GetSpikeDetectorNeuronIds);
}

SUPPRESS_WARNINGS_END

}  // namespace consumer
}  // namespace pynesci
