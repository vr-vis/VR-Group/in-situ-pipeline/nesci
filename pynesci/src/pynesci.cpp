//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "pynesci.hpp"
#include <string>
#include "conduit/conduit_node.hpp"
#include "nesci/data_type.hpp"

using boost::noncopyable;
using boost::python::args;
using boost::python::bases;
using boost::python::class_;
using boost::python::def;
using boost::python::enum_;
using boost::python::init;
using boost::python::no_init;
using boost::python::pure_virtual;
using boost::python::scope;
using boost::python::wrapper;

namespace pynesci {

namespace {
std::string Greet() { return "G'day!"; }
}  // namespace

SUPPRESS_WARNINGS_BEGIN
// cppcheck-suppress unusedFunction
BOOST_PYTHON_MODULE(_pynesci) {
  def("Greet", &Greet);

  enum_<nesci::DataType>("DataType")
      .value("Other", nesci::DataType::OTHER)
      .value("NestMultimeter", nesci::DataType::NEST_MULTIMETER)
      .value("SpikeDetector", nesci::DataType::SPIKE_DETECTOR);
}
SUPPRESS_WARNINGS_END

}  // namespace pynesci
