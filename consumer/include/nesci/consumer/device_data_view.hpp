//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef CONSUMER_INCLUDE_NESCI_CONSUMER_DEVICE_DATA_VIEW_HPP_
#define CONSUMER_INCLUDE_NESCI_CONSUMER_DEVICE_DATA_VIEW_HPP_

#include <string>
#include <vector>

#include "conduit/conduit_data_array.hpp"
#include "conduit/conduit_node.hpp"

#include "nesci/data_type.hpp"
#include "nesci/device_layout.hpp"

namespace conduit {

extern template class DataArray<uint64>;

}  // namespace conduit

namespace nesci {
namespace consumer {

class DeviceDataView {
 public:
  DeviceDataView() = delete;
  explicit inline DeviceDataView(const conduit::Node* node) : node_(node) {}
  DeviceDataView(const DeviceDataView&) = default;
  DeviceDataView(DeviceDataView&&) = default;
  virtual ~DeviceDataView() = default;

  DeviceDataView& operator=(const DeviceDataView&) = default;
  DeviceDataView& operator=(DeviceDataView&&) = default;

  bool HasName() const;
  std::string GetName() const;
  DataType GetType() const;

 protected:
  const conduit::Node* FetchPath(const std::string& path) const;

 private:
  const conduit::Node* node_{nullptr};
};

}  // namespace consumer
}  // namespace nesci

#endif  // CONSUMER_INCLUDE_NESCI_CONSUMER_DEVICE_DATA_VIEW_HPP_
