//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef CONSUMER_INCLUDE_NESCI_CONSUMER_NEST_MULTIMETER_DATA_VIEW_HPP_
#define CONSUMER_INCLUDE_NESCI_CONSUMER_NEST_MULTIMETER_DATA_VIEW_HPP_

#include <string>
#include <vector>

#include "nesci/consumer/device_data_view.hpp"

namespace nesci {
namespace consumer {

class NestMultimeterDataView : public DeviceDataView {
 public:
  NestMultimeterDataView() = delete;
  explicit NestMultimeterDataView(const conduit::Node* node);
  NestMultimeterDataView(const NestMultimeterDataView&) = default;
  NestMultimeterDataView(NestMultimeterDataView&&) = default;
  ~NestMultimeterDataView() = default;

  NestMultimeterDataView& operator=(const NestMultimeterDataView&) = default;
  NestMultimeterDataView& operator=(NestMultimeterDataView&&) = default;

  bool IsValid() const;
  double GetTimestep() const;
  conduit::uint64_array GetNeuronIds() const;
  std::vector<std::string> GetIntegerAttributeNames() const;
  std::vector<std::string> GetFloatingPointAttributeNames() const;
  conduit::int64_array GetIntegerAttributeValues(
      const std::string& attribute) const;
  conduit::float64_array GetFloatingPointAttributeValues(
      const std::string& attribute) const;
};

}  // namespace consumer
}  // namespace nesci

#endif  // CONSUMER_INCLUDE_NESCI_CONSUMER_NEST_MULTIMETER_DATA_VIEW_HPP_
