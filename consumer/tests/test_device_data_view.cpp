//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <string>
#include <vector>

#include "catch2/catch.hpp"

#include "conduit/conduit_node.hpp"
#include "nesci/consumer/device_data_view.hpp"
#include "nesci/data_type.hpp"
#include "nesci/testing/data.hpp"

SCENARIO("consumer::DeviceView") {
  GIVEN("a device providing access to some data") {
    conduit::Node n;
    n["info/deviceType"] =
        static_cast<conduit::uint64>(nesci::DataType::NEST_MULTIMETER);
    nesci::consumer::DeviceDataView device_view(&n);

    THEN("the device view returns the correct type") {
      REQUIRE(static_cast<uint64_t>(device_view.GetType()) ==
              static_cast<uint64_t>(nesci::DataType::NEST_MULTIMETER));
    }
  }
  GIVEN("a conduit node with invalid type") {
    conduit::Node n;
    n["info/deviceType"] = static_cast<conduit::uint64>(13131231);
    nesci::consumer::DeviceDataView device_view(&n);

    THEN("the device view returns the type OTHER") {
      REQUIRE(static_cast<uint64_t>(device_view.GetType()) ==
              static_cast<uint64_t>(nesci::DataType::OTHER));
    }
  }
}
