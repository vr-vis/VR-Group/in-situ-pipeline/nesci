//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <vector>

#include "catch2/catch.hpp"

#include "conduit/conduit_node.hpp"

#include "nesci/consumer/spike_detector_data_view.hpp"
#include "nesci/testing/data.hpp"

#include "utilities/vector_all_nan_or_empty.hpp"

SCENARIO("Test GetNeuronIds",
         "[nesci][nesci::consumer][nesci::consumer::SpikeDetectorDataView]") {
  GIVEN("a conduit node and a spike detector") {
    conduit::Node node = nesci::testing::CreateSpikeDetectorDataNode();
    nesci::consumer::SpikeDetectorDataView spike_detector_view{&node};

    THEN("the view is valid") { REQUIRE(spike_detector_view.IsValid()); }
    THEN("the device name is correct") {
      REQUIRE(spike_detector_view.HasName());
      REQUIRE(spike_detector_view.GetName() ==
              nesci::testing::ANY_SPIKE_DETECTOR_NAME);
    }
    THEN("the data is correct") {
      auto neuron_ids = spike_detector_view.GetNeuronIds();
      auto timesteps = spike_detector_view.GetTimesteps();

      REQUIRE(static_cast<size_t>(neuron_ids.number_of_elements()) ==
              nesci::testing::ANY_SPIKE_DATA.size());
      REQUIRE(static_cast<size_t>(timesteps.number_of_elements()) ==
              nesci::testing::ANY_SPIKE_DATA.size());

      for (size_t i = 0; i < nesci::testing::ANY_SPIKE_DATA.size(); ++i) {
        REQUIRE(neuron_ids[static_cast<conduit::index_t>(i)] ==
                nesci::testing::ANY_SPIKE_DATA[i].node_id);
        REQUIRE(timesteps[static_cast<conduit::index_t>(i)] ==
                nesci::testing::ANY_SPIKE_DATA[i].time);
      }
    }
  }
}
