//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "nesci/consumer/spike_detector_data_view.hpp"

#include <string>
#include <vector>
#include "nesci/spike_detector_layout.hpp"

namespace nesci {
namespace consumer {

namespace device_layout = nesci::layout::spike_detector;

SpikeDetectorDataView::SpikeDetectorDataView(const conduit::Node* node)
    : DeviceDataView{node} {}

bool SpikeDetectorDataView::IsValid() const {
  return GetType() == DataType::SPIKE_DETECTOR;
}

conduit::float64_array SpikeDetectorDataView::GetTimesteps() const {
  auto data_node = FetchPath(device_layout::GetTimesPath());
  return data_node != nullptr ? data_node->as_float64_array()
                              : conduit::float64_array{};
}

conduit::uint64_array SpikeDetectorDataView::GetNeuronIds() const {
  auto data_node = FetchPath(device_layout::GetNeuronIdsPath());
  return data_node != nullptr ? data_node->as_uint64_array()
                              : conduit::uint64_array{};
}

}  // namespace consumer
}  // namespace nesci
