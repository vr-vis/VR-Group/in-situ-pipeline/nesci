//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "nesci/consumer/device_data_view.hpp"

#include <cmath>
#include <cstdlib>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "nesci/device_layout.hpp"
#include "nesci/suppress_warnings.hpp"

namespace nesci {
namespace consumer {

namespace device_layout = nesci::layout::device;

bool DeviceDataView::HasName() const {
  return node_ != nullptr && node_->has_path(device_layout::GetNamePath());
}

std::string DeviceDataView::GetName() const {
  if (node_ == nullptr) {
    return "";
  }

  auto name_node = node_->fetch_ptr(device_layout::GetNamePath());
  if (name_node != nullptr) {
    return name_node->as_string();
  } else {
    return "";
  }
}

DataType DeviceDataView::GetType() const {
  if (node_ == nullptr) {
    return DataType::OTHER;
  }

  auto name_node = node_->fetch_ptr(device_layout::GetTypePath());
  if (name_node != nullptr) {
    const auto data_type = static_cast<DataType>(name_node->as_uint64());
    switch (data_type) {
      case DataType::SPIKE_DETECTOR:
      case DataType::NEST_MULTIMETER:
      case DataType::OTHER:
        return data_type;
    }
    return DataType::OTHER;
  } else {
    return DataType::OTHER;
  }
}

const conduit::Node* DeviceDataView::FetchPath(const std::string& path) const {
  return node_ ? node_->fetch_ptr(path) : nullptr;
}

}  // namespace consumer
}  // namespace nesci
