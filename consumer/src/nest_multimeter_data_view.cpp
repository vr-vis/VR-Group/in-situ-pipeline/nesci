//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "nesci/consumer/nest_multimeter_data_view.hpp"

#include <limits>
#include <stdexcept>
#include <string>
#include <vector>

#include "nesci/nest_multimeter_layout.hpp"

namespace nesci {
namespace consumer {

namespace device_layout = nesci::layout::nest_multimeter;

NestMultimeterDataView::NestMultimeterDataView(const conduit::Node* node)
    : DeviceDataView{node} {}

bool NestMultimeterDataView::IsValid() const {
  return GetType() == nesci::DataType::NEST_MULTIMETER;
}

double NestMultimeterDataView::GetTimestep() const {
  auto data_node = FetchPath(device_layout::GetTimestepPath());
  return data_node != nullptr ? data_node->as_double()
                              : std::numeric_limits<double>::quiet_NaN();
}

conduit::uint64_array NestMultimeterDataView::GetNeuronIds() const {
  auto data_node = FetchPath(device_layout::GetNeuronIdsPath());
  return data_node != nullptr ? data_node->as_uint64_array()
                              : conduit::uint64_array{};
}

std::vector<std::string>
NestMultimeterDataView::GetFloatingPointAttributeNames() const {
  std::vector<std::string> attributes;
  auto data_node = FetchPath(device_layout::GetAttributePath());
  if (data_node) {
    const auto& data_schema = data_node->schema();
    for (conduit::index_t i = 0; i < data_schema.number_of_children(); ++i) {
      const auto& child = data_schema.child(i);
      if (child.dtype().is_float64()) {
        attributes.push_back(data_schema.child_name(i));
      }
    }
  }
  return attributes;
}

conduit::float64_array NestMultimeterDataView::GetFloatingPointAttributeValues(
    const std::string& attribute) const {
  auto data_node = FetchPath(device_layout::GetPathForAttribute(attribute));
  return data_node != nullptr ? data_node->as_float64_array()
                              : conduit::float64_array{};
}

std::vector<std::string> NestMultimeterDataView::GetIntegerAttributeNames()
    const {
  std::vector<std::string> attributes;
  auto data_node = FetchPath(device_layout::GetAttributePath());
  if (data_node) {
    const auto& data_schema = data_node->schema();
    for (conduit::index_t i = 0; i < data_schema.number_of_children(); ++i) {
      const auto& child = data_schema.child(i);
      if (child.dtype().is_int64()) {
        attributes.push_back(data_schema.child_name(i));
      }
    }
  }
  return attributes;
}

conduit::int64_array NestMultimeterDataView::GetIntegerAttributeValues(
    const std::string& attribute) const {
  auto data_node = FetchPath(device_layout::GetPathForAttribute(attribute));
  return data_node != nullptr ? data_node->as_int64_array()
                              : conduit::int64_array{};
}

}  // namespace consumer
}  // namespace nesci
