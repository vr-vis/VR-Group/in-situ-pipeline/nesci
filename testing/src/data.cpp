//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "nesci/testing/data.hpp"

#include <sstream>

namespace nesci {
namespace testing {

template <typename T>
std::string AttributeEntry(const std::string& name, const std::string& type,
                           const std::vector<T>& values) {
  std::stringstream stream;
  stream << name << ": "
         << "{dtype: " << type << ", value: [";
  for (size_t i = 0; i < values.size(); ++i) {
    stream << values[i];
    if (i != values.size() - 1) {
      stream << ", ";
    }
  }
  stream << "]}";
  return stream.str();
}

conduit::Node CreateNestMultimeterDataNode() {
  std::stringstream stream;
  stream << "{"
            "  info: {"
            "    deviceType: {dtype:uint64, value: 2},"
            "    deviceName: {dtype:char8_str, value: \""
         << ANY_MULTIMETER_NAME
         << "\"}"
            "  },"
            "  neuronIds: {"
            "    dtype:uint64, value: ["
         << ANY_ID << "," << OTHER_ID
         << "]"
            "  },"
            "  attributes: {"
         << AttributeEntry<double>(
                ANY_DOUBLE_ATTRIBUTES[0], "float64",
                {ANY_DOUBLE_VALUES[0], OTHER_DOUBLE_VALUES[0]})
         << ", "
         << AttributeEntry<double>(
                ANY_DOUBLE_ATTRIBUTES[1], "float64",
                {ANY_DOUBLE_VALUES[1], OTHER_DOUBLE_VALUES[1]})
         << ", "
         << AttributeEntry<long>(ANY_LONG_ATTRIBUTES[0], "int64",  // NOLINT
                                 {ANY_LONG_VALUES[0], OTHER_LONG_VALUES[0]})
         << ", "
         << AttributeEntry<long>(ANY_LONG_ATTRIBUTES[1], "int64",  // NOLINT
                                 {ANY_LONG_VALUES[1], OTHER_LONG_VALUES[1]})
         << "  }"
            "}";

  std::cout << stream.str() << std::endl;

  return {conduit::Generator{stream.str(), "conduit_json"}, false};
}

conduit::Node CreateSpikeDetectorDataNode() {
  std::stringstream stream;
  stream << "{"
            "  info: {"
            "    deviceType: {dtype:uint64, value: 1},"
            "    deviceName: {dtype:char8_str, value: \""
         << ANY_SPIKE_DETECTOR_NAME
         << "\"}"
            "  },"
            "  neuronIds: {"
            "    dtype:uint64, value: [";

  for (size_t i = 0; i < ANY_SPIKE_DATA.size(); ++i) {
    stream << ANY_SPIKE_DATA[i].node_id;
    if (i != ANY_SPIKE_DATA.size() - 1) {
      stream << ", ";
    }
  }

  stream << "]"
            "  },"
            "  times: {"
            "    dtype:float64, value: [";

  for (size_t i = 0; i < ANY_SPIKE_DATA.size(); ++i) {
    stream << ANY_SPIKE_DATA[i].time;
    if (i != ANY_SPIKE_DATA.size() - 1) {
      stream << ", ";
    }
  }

  stream << "]"
            "  }"
            "}";

  std::cout << stream.str() << std::endl;

  return {conduit::Generator{stream.str(), "conduit_json"}, false};
}

}  // namespace testing
}  // namespace nesci
