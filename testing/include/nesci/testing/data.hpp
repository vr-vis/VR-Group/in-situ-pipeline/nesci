//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTING_INCLUDE_NESCI_TESTING_DATA_HPP_
#define TESTING_INCLUDE_NESCI_TESTING_DATA_HPP_

#include <cmath>

#include <limits>
#include <sstream>
#include <string>
#include <vector>

#include "conduit/conduit_node.hpp"
#include "nesci/testing/conduit_schema.hpp"

#if defined(__GNUC__) && !defined(__clang__)
#define NESCI_UNUSED __attribute__((unused))
#else
#define NESCI_UNUSED
#endif

namespace nesci {
namespace testing {

NESCI_UNUSED const std::string ANY_MULTIMETER_NAME{"AwesomeMultimeter"};
NESCI_UNUSED const std::vector<std::string> ANY_DOUBLE_ATTRIBUTES = {"V_m",
                                                                     "g_e"};
NESCI_UNUSED const std::vector<std::string> ANY_LONG_ATTRIBUTES = {"Att0",
                                                                   "Att1"};

NESCI_UNUSED const double ANY_TIME = 0.1;
NESCI_UNUSED const double OTHER_TIME = 0.2;

NESCI_UNUSED const uint64_t ANY_ID = 1;
NESCI_UNUSED const uint64_t OTHER_ID = 13;

NESCI_UNUSED const std::vector<double> ANY_DOUBLE_VALUES = {-0.2, 0.5};
NESCI_UNUSED const std::vector<double> OTHER_DOUBLE_VALUES = {123.0, -0.13};
NESCI_UNUSED const std::vector<long> ANY_LONG_VALUES = {-2, 5};    // NOLINT
NESCI_UNUSED const std::vector<long> OTHER_LONG_VALUES = {123897,  // NOLINT
                                                          -238};

conduit::Node CreateNestMultimeterDataNode();

NESCI_UNUSED const std::string ANY_SPIKE_DETECTOR_NAME = "AwesomeSpikeDetector";
struct SpikeData {
  double time;
  uint64_t node_id;
};
NESCI_UNUSED const std::vector<SpikeData> ANY_SPIKE_DATA = {
    {0.1, 10}, {0.2, 123}, {0.2, 12}, {0.3, 12}};

conduit::Node CreateSpikeDetectorDataNode();

}  // namespace testing
}  // namespace nesci

#endif  // TESTING_INCLUDE_NESCI_TESTING_DATA_HPP_
