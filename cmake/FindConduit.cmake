#-------------------------------------------------------------------------------
# nesci -- neuronal simulator conan interface
#
# Copyright (c) 2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#-------------------------------------------------------------------------------
#                                  License
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

find_file(
  Conduit_CONFIG_FILE
  "conduit.cmake"
  HINTS
    ${Conduit_DIR}
    ${CONDUIT_DIR}/lib/cmake  # For backwards comability
  PATH_SUFFIXES
    lib/cmake
)

find_path(
  Conduit_INCLUDE_DIRECTORIES
  "conduit/conduit.h"
  HINTS
    ${Conduit_DIR}/../../include
    ${CONDUIT_DIR}/include  # For backwards comability
)

mark_as_advanced(
  Conduit_CONFIG_FILE
  Conduit_INCLUDE_DIRECTORIES
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Conduit
  FOUND_VAR Conduit_FOUND
  REQUIRED_VARS
    Conduit_CONFIG_FILE
    Conduit_INCLUDE_DIRECTORIES
)

if (Conduit_FOUND AND NOT TARGET conduit)
  include(${Conduit_CONFIG_FILE})
  set_target_properties(conduit
    PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES ${Conduit_INCLUDE_DIRECTORIES}
  )
endif ()
