//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef NESCI_INCLUDE_NESCI_DEVICE_LAYOUT_HPP_
#define NESCI_INCLUDE_NESCI_DEVICE_LAYOUT_HPP_

#include <string>

namespace nesci {
namespace layout {
namespace device {

inline std::string GetTypePath() { return "info/deviceType"; }
inline std::string GetNamePath() { return "info/deviceName"; }

}  // namespace device
}  // namespace layout
}  // namespace nesci

#endif  // NESCI_INCLUDE_NESCI_DEVICE_LAYOUT_HPP_
