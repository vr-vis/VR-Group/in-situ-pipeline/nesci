//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>
#include <vector>

#include "nesci/producer/spike_detector.hpp"
#include "nesci/spike_detector_layout.hpp"

namespace nesci {
namespace producer {

namespace device_layout = nesci::layout::spike_detector;

SpikeDetector::SpikeDetector(const std::string& name)
    : Device{DataType::SPIKE_DETECTOR, 0, sizeof(Datum), name} {
  conduit::index_t current_offset =
      static_cast<conduit::index_t>(GetRecordOffset());
  const conduit::index_t record_stride =
      static_cast<conduit::index_t>(GetRecordStride());
  schema()[device_layout::GetTimesPath()] =
      conduit::DataType::float64(0, current_offset, record_stride);
  current_offset += 8;
  schema()[device_layout::GetNeuronIdsPath()] =
      conduit::DataType::uint64(0, current_offset, record_stride);  // NOLINT
}

void SpikeDetector::Record(double time, uint64_t node_id) {
  auto datum = PrepareRecord<Datum>();
  datum->time = time;
  datum->node_id = node_id;

  conduit::index_t record_count =
      static_cast<conduit::index_t>(GetRecordCount());
  schema()[device_layout::GetNeuronIdsPath()].dtype().set_number_of_elements(
      record_count);
  schema()[device_layout::GetTimesPath()].dtype().set_number_of_elements(
      record_count);
  FinalizeRecords();
}

}  // namespace producer
}  // namespace nesci
