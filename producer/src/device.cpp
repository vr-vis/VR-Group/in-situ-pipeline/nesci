//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <sstream>
#include <string>
#include <vector>

#include "nesci/device_layout.hpp"
#include "nesci/producer/device.hpp"

namespace nesci {
namespace producer {

Device::Device(DataType data_type, size_t additional_info_size,
               size_t record_size, std::string name)
    : data_type_(data_type),
      name_(std::move(name)),
      general_info_size_(8),
      additional_info_size_(additional_info_size),
      record_size_(record_size),
      record_count_(0) {
  assert(additional_info_size % 8 == 0);
  assert(record_size % 8 == 0);
  buffer_.resize(general_info_size_ + additional_info_size_);

  schema_[nesci::layout::device::GetTypePath()] = conduit::DataType::uint64();
  *GetGeneralInfoData<DataType>() = data_type;
}

void Device::FinalizeRecords() { node_.set_external(schema_, buffer_.data()); }

void Device::Clear() {
  node_.reset();
  record_count_ = 0;
}

void* Device::PrepareRecordsInternal(size_t new_record_count) {
  record_count_ += new_record_count;
  assert((general_info_size_ + additional_info_size_ +
          record_count_ * record_size_) %
             8 ==
         0);
  buffer_.resize((general_info_size_ + additional_info_size_ +
                  record_count_ * record_size_) /
                 8);
  return reinterpret_cast<void*>(
      buffer_.data() + (general_info_size_ + additional_info_size_ +
                        (record_count_ - new_record_count) * record_size_) /
                           8);
}

}  // namespace producer
}  // namespace nesci
