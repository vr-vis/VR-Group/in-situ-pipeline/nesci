//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <cassert>
#include <cmath>

#include <limits>
#include <memory>
#include <string>
#include <vector>

#include "conduit/conduit_data_type.hpp"
#include "nesci/nest_multimeter_layout.hpp"
#include "nesci/producer/nest_multimeter.hpp"

namespace nesci {
namespace producer {

namespace device_layout = nesci::layout::nest_multimeter;

NestMultimeter::NestMultimeter(std::string device_name,
                               std::vector<std::string> double_value_names,
                               std::vector<std::string> long_value_names,
                               size_t)
    : Device(DataType::NEST_MULTIMETER, 8,
             8 * (1 + double_value_names.size() + long_value_names.size()),
             device_name),
      double_value_names_{std::move(double_value_names)},
      long_value_names_{std::move(long_value_names)} {
  schema()[device_layout::GetTimestepPath()] = conduit::DataType::float64(
      1, static_cast<conduit::index_t>(GetAdditionalInfoOffset()));

  conduit::index_t current_offset =
      static_cast<conduit::index_t>(GetRecordOffset());
  const conduit::index_t record_stride =
      static_cast<conduit::index_t>(GetRecordStride());

  schema()[device_layout::GetNeuronIdsPath()] =
      conduit::DataType::uint64(0, current_offset, record_stride);  // NOLINT
  current_offset += 8;

  for (const auto& name : double_value_names_) {
    schema()[device_layout::GetPathForAttribute(name)] =
        conduit::DataType::float64(0, current_offset, record_stride);
    current_offset += 8;
  }

  for (const auto& name : long_value_names_) {
    schema()[device_layout::GetPathForAttribute(name)] =
        conduit::DataType::int64(0, current_offset, record_stride);  // NOLINT
    current_offset += 8;
  }
}

void NestMultimeter::Record(double time, uint64_t node_id,
                            const double* double_values,
                            const long* long_values) {  // NOLINT
  if (GetRecordCount() > 0 && GetTimestep() != time) {
    throw std::runtime_error(
        "The device data needs to be cleared before changing the timestep.");
  }
  if (GetRecordCount() == 0) {
    SetTimestep(time);
  }

  auto record_pointer = PrepareRecord();
  conduit::index_t record_count =
      static_cast<conduit::index_t>(GetRecordCount());

  // Set node id
  *reinterpret_cast<uint64_t*>(record_pointer) = node_id;
  schema()[device_layout::GetNeuronIdsPath()].dtype().set_number_of_elements(
      record_count);
  record_pointer += 8;

  // Set double values
  for (size_t i = 0; i < double_value_names_.size(); ++i) {
    *reinterpret_cast<double*>(record_pointer) = double_values[i];
    schema()[device_layout::GetPathForAttribute(double_value_names_[i])]
        .dtype()
        .set_number_of_elements(record_count);
    record_pointer += 8;
  }

  // Set long values
  for (size_t i = 0; i < long_value_names_.size(); ++i) {
    *reinterpret_cast<int64_t*>(record_pointer) = long_values[i];
    schema()[device_layout::GetPathForAttribute(long_value_names_[i])]
        .dtype()
        .set_number_of_elements(record_count);
    record_pointer += 8;
  }

  FinalizeRecords();
}

double NestMultimeter::GetTimestep() {
  return *GetAdditionalInfoData<double>();
}

void NestMultimeter::SetTimestep(double timestep) {
  *GetAdditionalInfoData<double>() = timestep;
}

}  // namespace producer
}  // namespace nesci
