//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>
#include <vector>

#include "catch2/catch.hpp"

#include "conduit/conduit_node.hpp"

#include "nesci/nest_multimeter_layout.hpp"
#include "nesci/producer/nest_multimeter.hpp"
#include "nesci/testing/data.hpp"

namespace device_layout = nesci::layout::nest_multimeter;

SCENARIO("A nest multimeter records to a conduit node",
         "[nesci][nesci::NestMultimeter]") {
  GIVEN("A conduit node and a multimeter") {
    nesci::producer::NestMultimeter multimeter{
        nesci::testing::ANY_MULTIMETER_NAME,
        nesci::testing::ANY_DOUBLE_ATTRIBUTES,
        nesci::testing::ANY_LONG_ATTRIBUTES};
    WHEN("recording data") {
      multimeter.Record(nesci::testing::ANY_TIME, nesci::testing::ANY_ID,
                        nesci::testing::ANY_DOUBLE_VALUES.data(),
                        nesci::testing::ANY_LONG_VALUES.data());
      conduit::Node node = multimeter.node();
      std::cout << "Node after first record";
      node.print();

      THEN("the node is compact") { REQUIRE(node.is_compact() == true); }

      THEN("data is properly recorded") {
        REQUIRE(node[device_layout::GetNeuronIdsPath()].as_uint64() ==
                nesci::testing::ANY_ID);

        for (size_t i = 0; i < nesci::testing::ANY_DOUBLE_ATTRIBUTES.size();
             ++i) {
          REQUIRE(node[device_layout::GetPathForAttribute(
                           nesci::testing::ANY_DOUBLE_ATTRIBUTES[i])]
                      .as_double() ==
                  Approx(nesci::testing::ANY_DOUBLE_VALUES[i]));
        }

        for (size_t i = 0; i < nesci::testing::ANY_LONG_ATTRIBUTES.size();
             ++i) {
          REQUIRE(node[device_layout::GetPathForAttribute(
                           nesci::testing::ANY_LONG_ATTRIBUTES[i])]
                      .as_int64() ==
                  Approx(nesci::testing::ANY_LONG_VALUES[i]));
        }
      }
      WHEN("recording additional data") {
        multimeter.Record(nesci::testing::ANY_TIME, nesci::testing::OTHER_ID,
                          nesci::testing::OTHER_DOUBLE_VALUES.data(),
                          nesci::testing::OTHER_LONG_VALUES.data());
        node = multimeter.node();
        std::cout << "Node after second record";
        node.print();

        THEN("the node is compact") { REQUIRE(node.is_compact() == true); }

        const auto ids =
            node[device_layout::GetNeuronIdsPath()].as_uint64_array();

        THEN("data is properly recorded") {
          REQUIRE(ids.number_of_elements() == 2);
          REQUIRE(ids[1] == nesci::testing::OTHER_ID);

          for (size_t i = 0; i < nesci::testing::ANY_DOUBLE_ATTRIBUTES.size();
               ++i) {
            const auto values =
                node[device_layout::GetPathForAttribute(
                         nesci::testing::ANY_DOUBLE_ATTRIBUTES[i])]
                    .as_double_array();
            REQUIRE(values.number_of_elements() == 2);
            REQUIRE(values[1] ==
                    Approx(nesci::testing::OTHER_DOUBLE_VALUES[i]));
          }

          for (size_t i = 0; i < nesci::testing::ANY_LONG_ATTRIBUTES.size();
               ++i) {
            const auto values =
                node[device_layout::GetPathForAttribute(
                         nesci::testing::ANY_LONG_ATTRIBUTES[i])]
                    .as_int64_array();
            REQUIRE(values.number_of_elements() == 2);
            REQUIRE(values[1] == Approx(nesci::testing::OTHER_LONG_VALUES[i]));
          }
        }

        THEN("the old data is still there") {
          REQUIRE(node[device_layout::GetNeuronIdsPath()].as_uint64() ==
                  nesci::testing::ANY_ID);

          for (size_t i = 0; i < nesci::testing::ANY_DOUBLE_ATTRIBUTES.size();
               ++i) {
            REQUIRE(node[device_layout::GetPathForAttribute(
                             nesci::testing::ANY_DOUBLE_ATTRIBUTES[i])]
                        .as_double() ==
                    Approx(nesci::testing::ANY_DOUBLE_VALUES[i]));
          }

          for (size_t i = 0; i < nesci::testing::ANY_LONG_ATTRIBUTES.size();
               ++i) {
            REQUIRE(node[device_layout::GetPathForAttribute(
                             nesci::testing::ANY_LONG_ATTRIBUTES[i])]
                        .as_int64() == nesci::testing::ANY_LONG_VALUES[i]);
          }
        }
      }
    }
  }
}
