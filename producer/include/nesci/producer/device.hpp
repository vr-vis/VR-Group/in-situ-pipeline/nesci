//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef PRODUCER_INCLUDE_NESCI_PRODUCER_DEVICE_HPP_
#define PRODUCER_INCLUDE_NESCI_PRODUCER_DEVICE_HPP_

#include <cassert>
#include <string>
#include <type_traits>
#include <vector>

#include "nesci/producer/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "conduit/conduit_node.hpp"
SUPPRESS_WARNINGS_END

#include "nesci/data_type.hpp"

namespace conduit {

template <>
DataArray<uint64>::~DataArray();

}  // namespace conduit

namespace nesci {
namespace producer {

class Device {
 public:
  Device() = delete;
  Device(const Device&) = default;
  Device(Device&&) = default;
  virtual ~Device() = default;

  Device& operator=(const Device&) = default;
  Device& operator=(Device&&) = default;

  inline conduit::Node& node() { return node_; }
  inline const conduit::Node& node() const { return node_; }

  virtual void Clear();

  inline size_t GetRecordCount() { return record_count_; }
  inline DataType GetDataType() { return data_type_; }

 protected:
  Device(DataType data_type, size_t additional_info_size, size_t record_size,
         std::string name = "");

  const std::string& GetName() { return name_; }

  inline size_t GetAdditionalInfoOffset() { return general_info_size_; }
  inline size_t GetRecordOffset() {
    return general_info_size_ + additional_info_size_;
  }
  inline size_t GetRecordStride() { return record_size_; }
  inline conduit::Schema& schema() { return schema_; }

  template <typename T = uint8_t>
  T* PrepareRecord() {
    return PrepareRecords<T>(1);
  }

  template <typename T = uint8_t>
  T* PrepareRecords(size_t record_count) {
    assert(sizeof(T) == 1 || sizeof(T) == record_size_);
    return reinterpret_cast<T*>(PrepareRecordsInternal(record_count));
  }
  void FinalizeRecords();

  template <typename T>
  inline T* GetAdditionalInfoData(size_t offset = 0) {
    assert(offset + sizeof(T) <= additional_info_size_);
    assert(offset % 8 == 0);
    return reinterpret_cast<T*>(buffer_.data() +
                                (general_info_size_ + offset) / 8);
  }

 private:
  DataType data_type_;
  std::string name_;
  conduit::Node node_;
  size_t general_info_size_;
  size_t additional_info_size_;
  size_t record_size_;
  size_t record_count_;
  std::vector<uint64_t> buffer_;
  conduit::Schema schema_;

  void* PrepareRecordsInternal(size_t record_count);

  template <typename T>
  inline T* GetGeneralInfoData(size_t offset = 0) {
    assert(offset + sizeof(T) <= general_info_size_);
    assert(offset % 8 == 0);
    return reinterpret_cast<T*>(buffer_.data() + offset / 8);
  }
};

}  // namespace producer
}  // namespace nesci

#endif  // PRODUCER_INCLUDE_NESCI_PRODUCER_DEVICE_HPP_
