//------------------------------------------------------------------------------
// nesci -- neuronal simulator conan interface
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef PRODUCER_INCLUDE_NESCI_PRODUCER_NEST_MULTIMETER_HPP_
#define PRODUCER_INCLUDE_NESCI_PRODUCER_NEST_MULTIMETER_HPP_

#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "nesci/producer/device.hpp"

static_assert(sizeof(double) == 8, "");

namespace nesci {
namespace producer {

class NestMultimeter final : public Device {
 public:
  NestMultimeter() = delete;
  NestMultimeter(std::string device_name,
                 std::vector<std::string> double_value_names,
                 std::vector<std::string> long_value_names,
                 size_t estimated_node_count = 100);
  NestMultimeter(const NestMultimeter&) = default;
  NestMultimeter(NestMultimeter&&) = default;
  ~NestMultimeter() override = default;

  NestMultimeter& operator=(const NestMultimeter&) = default;
  NestMultimeter& operator=(NestMultimeter&&) = default;

  void Record(double time, uint64_t node_id, const double* double_values,
              const long* long_values);  // NOLINT

 private:
  std::vector<std::string> double_value_names_;
  std::vector<std::string> long_value_names_;

  double GetTimestep();
  void SetTimestep(double timestep);
};

}  // namespace producer
}  // namespace nesci

#endif  // PRODUCER_INCLUDE_NESCI_PRODUCER_NEST_MULTIMETER_HPP_
